const fs = require('fs')
const util = require('util')
const pathDirSource = './fuente/'
const pathFileTarget = '../directory/config_test/itxenv/setup/mongodb/meccilblpl/itxtype/scripts/itxfilename.json'
//const ambientes = ["integration"];
const ambientes = ["contract", "integration", "observability", "performance"];
const readdir = util.promisify(fs.readdir);
let pathFileSource = '';
async function myF(ambiente) {
    let names;
    try {
        names = await readdir(pathDirSource);
    } catch (err) {
        console.log(err);
    }
    if (names == undefined) {
        console.log('undefined');
    } else {
        console.log('First Name', names[0]);
        if (names[0]) {
            pathFileSource = pathDirSource + names[0];
            fs.readFile(pathFileSource, (errRead, data) => {
                if (errRead)
                    console.log(errRead);
                else {
                    let bufferOriginal = data;
                    let content = JSON.parse(bufferOriginal.toString('utf8'));
                    let entitiesMongoArray = Object.keys(content);
                    let entitiesMongoArrayAttribute = Object.keys(content);
                    entitiesMongoArray = entitiesMongoArray.map(t => {
                        let lastIndex = t.lastIndexOf(".");
                        let final = t.substring(lastIndex + 1, t.length);
                        return final.charAt(0).toLowerCase() + final.slice(1);
                    });
                    entitiesMongoArray.forEach((tipo, index) => {
                        let atributo = entitiesMongoArrayAttribute[index];
                        let dataTemp = content[atributo];
                        let stringFinal = "";
                        dataTemp.forEach(x => {
                            stringFinal = stringFinal + "\n" + JSON.stringify(x, undefined, 2);
                        });
                        stringFinal = stringFinal.substring(1);
                        stringFinal = stringFinal.replace(/"_id": "|"id": "/gi, '"_id": ObjectId("');
                        stringFinal = stringFinal.replace(/(\bObjectId\("[a-z0-9]{24}")/g, '$&)');
                        let tempAmbiente = "/" + ambiente + "/";
                        let campos = names[0].replace('.json', '');
                        let camposArray = campos.split('$');
                        let courierId = camposArray[0];
                        let directory = camposArray[1];
                        let fileName = courierId + "-" + tipo + "Array";
                        if (atributo.includes("courierRoute_")) {
                            fileName = "courierRouteArray";
                        } else if (atributo.includes("courierSequence_")) {
                            fileName = "courierSequencesArray";
                        }
                        let pathFileTargetTemp = pathFileTarget.replace('itxtype', tipo);
                        pathFileTargetTemp = pathFileTargetTemp.replace('itxfilename', fileName);
                        pathFileTargetTemp = pathFileTargetTemp.replace('directory', directory);
                        let newPathFile = pathFileTargetTemp.replace('/itxenv/', tempAmbiente);
                        fs.writeFile(newPathFile, '', function() {
                            console.log('done')
                        });
                        let lastIndex2 = newPathFile.lastIndexOf("/");
                        let newPathFolder = newPathFile.slice(0, lastIndex2 + 1);
                        if (newPathFolder.includes("courierSequence")) {
                            let lastIndex3 = atributo.indexOf("#");
                            let nameFolder = atributo.slice(0, lastIndex3);
                            newPathFolder = newPathFolder.replace("courierSequence", nameFolder);
                            newPathFile = newPathFile.replace("courierSequence", nameFolder);
                        }
                        console.log("newPathFolder: " + newPathFolder);
                        console.log("newPathFile: " + newPathFile);
                        if (!fs.existsSync(newPathFolder)) {
                            fs.mkdirSync(newPathFolder, {
                                recursive: true
                            });
                        }
                        fs.writeFile(newPathFile, '', function() {
                            console.log('done')
                        });
                        fs.open(newPathFile, "a", (errOpen, fd) => {
                            if (errOpen) {
                                console.log(errOpen.message);
                            } else {
                                fs.write(fd, stringFinal, (errWrite, bytes) => {
                                    if (errWrite) {
                                        console.log("#ERROR# atributo:[" + atributo + "], ambiente:[" + ambiente + "], " + "tipo:[" + tipo + "]");
                                        console.log(errWrite.message);
                                    } else {
                                        console.log(bytes + ' bytes written - SUCCESS!!');
                                    }
                                })
                            }
                        });
                    })
                }
            })
        }
    }
}
ambientes.forEach(x => {
    myF(x);
})